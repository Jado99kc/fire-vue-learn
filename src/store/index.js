import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    posts:[],

  },
  mutations: {
    add_post(state, payload){
      state.posts.push(payload)
    }
  },
  actions: {
    addPost({commit}, newPost){
      commit('add', newPost)
    }
  },
  modules: {
  }
})
